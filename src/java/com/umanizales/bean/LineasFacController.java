package com.umanizales.bean;

import com.umanizales.entidad.LineasFac;
import com.umanizales.bean.util.JsfUtil;
import com.umanizales.bean.util.JsfUtil.PersistAction;
import com.umanizales.facade.LineasFacFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("lineasFacController")
@SessionScoped
public class LineasFacController implements Serializable {

    @EJB
    private com.umanizales.facade.LineasFacFacade ejbFacade;
    private List<LineasFac> items = null;
    private LineasFac selected;

    public LineasFacController() {
    }

    public LineasFac getSelected() {
        return selected;
    }

    public void setSelected(LineasFac selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
        selected.getLineasFacPK().setCodfac(selected.getFacturas().getCodfac());
    }

    protected void initializeEmbeddableKey() {
        selected.setLineasFacPK(new com.umanizales.entidad.LineasFacPK());
    }

    private LineasFacFacade getFacade() {
        return ejbFacade;
    }

    public LineasFac prepareCreate() {
        selected = new LineasFac();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("LineasFacCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("LineasFacUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("LineasFacDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<LineasFac> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public LineasFac getLineasFac(com.umanizales.entidad.LineasFacPK id) {
        return getFacade().find(id);
    }

    public List<LineasFac> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<LineasFac> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = LineasFac.class)
    public static class LineasFacControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            LineasFacController controller = (LineasFacController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "lineasFacController");
            return controller.getLineasFac(getKey(value));
        }

        com.umanizales.entidad.LineasFacPK getKey(String value) {
            com.umanizales.entidad.LineasFacPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new com.umanizales.entidad.LineasFacPK();
            key.setCodfac(Integer.parseInt(values[0]));
            key.setLinea(Short.parseShort(values[1]));
            return key;
        }

        String getStringKey(com.umanizales.entidad.LineasFacPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getCodfac());
            sb.append(SEPARATOR);
            sb.append(value.getLinea());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof LineasFac) {
                LineasFac o = (LineasFac) object;
                return getStringKey(o.getLineasFacPK());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), LineasFac.class.getName()});
                return null;
            }
        }

    }

}
