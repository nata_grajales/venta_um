/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umanizales.facade;

import com.umanizales.entidad.LineasFac;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author ngrajales
 */
@Stateless
public class LineasFacFacade extends AbstractFacade<LineasFac> {

    @PersistenceContext(unitName = "venta_umPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LineasFacFacade() {
        super(LineasFac.class);
    }
    
}
