/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umanizales.entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author ngrajales
 */
@Embeddable
public class LineasFacPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "codfac")
    private int codfac;
    @Basic(optional = false)
    @NotNull
    @Column(name = "linea")
    private short linea;

    public LineasFacPK() {
    }

    public LineasFacPK(int codfac, short linea) {
        this.codfac = codfac;
        this.linea = linea;
    }

    public int getCodfac() {
        return codfac;
    }

    public void setCodfac(int codfac) {
        this.codfac = codfac;
    }

    public short getLinea() {
        return linea;
    }

    public void setLinea(short linea) {
        this.linea = linea;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) codfac;
        hash += (int) linea;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LineasFacPK)) {
            return false;
        }
        LineasFacPK other = (LineasFacPK) object;
        if (this.codfac != other.codfac) {
            return false;
        }
        if (this.linea != other.linea) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.umanizales.entidad.LineasFacPK[ codfac=" + codfac + ", linea=" + linea + " ]";
    }
    
}
