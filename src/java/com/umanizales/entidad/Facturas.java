/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umanizales.entidad;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ngrajales
 */
@Entity
@Table(name = "facturas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Facturas.findAll", query = "SELECT f FROM Facturas f")
    , @NamedQuery(name = "Facturas.findByCodfac", query = "SELECT f FROM Facturas f WHERE f.codfac = :codfac")
    , @NamedQuery(name = "Facturas.findByFecha", query = "SELECT f FROM Facturas f WHERE f.fecha = :fecha")
    , @NamedQuery(name = "Facturas.findByIva", query = "SELECT f FROM Facturas f WHERE f.iva = :iva")
    , @NamedQuery(name = "Facturas.findByDto", query = "SELECT f FROM Facturas f WHERE f.dto = :dto")})
public class Facturas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codfac")
    private Integer codfac;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Column(name = "iva")
    private Short iva;
    @Column(name = "dto")
    private Short dto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "facturas")
    private List<LineasFac> lineasFacList;
    @JoinColumn(name = "codcli", referencedColumnName = "codcli")
    @ManyToOne
    private Clientes codcli;
    @JoinColumn(name = "codven", referencedColumnName = "codven")
    @ManyToOne
    private Vendedores codven;

    public Facturas() {
    }

    public Facturas(Integer codfac) {
        this.codfac = codfac;
    }

    public Facturas(Integer codfac, Date fecha) {
        this.codfac = codfac;
        this.fecha = fecha;
    }

    public Integer getCodfac() {
        return codfac;
    }

    public void setCodfac(Integer codfac) {
        this.codfac = codfac;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Short getIva() {
        return iva;
    }

    public void setIva(Short iva) {
        this.iva = iva;
    }

    public Short getDto() {
        return dto;
    }

    public void setDto(Short dto) {
        this.dto = dto;
    }

    @XmlTransient
    public List<LineasFac> getLineasFacList() {
        return lineasFacList;
    }

    public void setLineasFacList(List<LineasFac> lineasFacList) {
        this.lineasFacList = lineasFacList;
    }

    public Clientes getCodcli() {
        return codcli;
    }

    public void setCodcli(Clientes codcli) {
        this.codcli = codcli;
    }

    public Vendedores getCodven() {
        return codven;
    }

    public void setCodven(Vendedores codven) {
        this.codven = codven;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codfac != null ? codfac.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Facturas)) {
            return false;
        }
        Facturas other = (Facturas) object;
        if ((this.codfac == null && other.codfac != null) || (this.codfac != null && !this.codfac.equals(other.codfac))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return codfac.toString();
    }
    
}
