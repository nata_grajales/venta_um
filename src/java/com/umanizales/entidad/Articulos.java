/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umanizales.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ngrajales
 */
@Entity
@Table(name = "articulos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Articulos.findAll", query = "SELECT a FROM Articulos a")
    , @NamedQuery(name = "Articulos.findByCodart", query = "SELECT a FROM Articulos a WHERE a.codart = :codart")
    , @NamedQuery(name = "Articulos.findByDescrip", query = "SELECT a FROM Articulos a WHERE a.descrip = :descrip")
    , @NamedQuery(name = "Articulos.findByPrecio", query = "SELECT a FROM Articulos a WHERE a.precio = :precio")
    , @NamedQuery(name = "Articulos.findByStock", query = "SELECT a FROM Articulos a WHERE a.stock = :stock")
    , @NamedQuery(name = "Articulos.findByStockMin", query = "SELECT a FROM Articulos a WHERE a.stockMin = :stockMin")
    , @NamedQuery(name = "Articulos.findByDto", query = "SELECT a FROM Articulos a WHERE a.dto = :dto")})
public class Articulos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 8)
    @Column(name = "codart")
    private String codart;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "descrip")
    private String descrip;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio")
    private BigDecimal precio;
    @Column(name = "stock")
    private Integer stock;
    @Column(name = "stock_min")
    private Integer stockMin;
    @Column(name = "dto")
    private Short dto;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codart")
    private List<LineasFac> lineasFacList;

    public Articulos() {
    }

    public Articulos(String codart) {
        this.codart = codart;
    }

    public Articulos(String codart, String descrip, BigDecimal precio) {
        this.codart = codart;
        this.descrip = descrip;
        this.precio = precio;
    }

    public String getCodart() {
        return codart;
    }

    public void setCodart(String codart) {
        this.codart = codart;
    }

    public String getDescrip() {
        return descrip;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Integer getStockMin() {
        return stockMin;
    }

    public void setStockMin(Integer stockMin) {
        this.stockMin = stockMin;
    }

    public Short getDto() {
        return dto;
    }

    public void setDto(Short dto) {
        this.dto = dto;
    }

    @XmlTransient
    public List<LineasFac> getLineasFacList() {
        return lineasFacList;
    }

    public void setLineasFacList(List<LineasFac> lineasFacList) {
        this.lineasFacList = lineasFacList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codart != null ? codart.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Articulos)) {
            return false;
        }
        Articulos other = (Articulos) object;
        if ((this.codart == null && other.codart != null) || (this.codart != null && !this.codart.equals(other.codart))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.umanizales.entidad.Articulos[ codart=" + codart + " ]";
    }
    
}
