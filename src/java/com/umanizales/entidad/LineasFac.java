/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umanizales.entidad;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ngrajales
 */
@Entity
@Table(name = "lineas_fac")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LineasFac.findAll", query = "SELECT l FROM LineasFac l")
    , @NamedQuery(name = "LineasFac.findByCodfac", query = "SELECT l FROM LineasFac l WHERE l.lineasFacPK.codfac = :codfac")
    , @NamedQuery(name = "LineasFac.findByLinea", query = "SELECT l FROM LineasFac l WHERE l.lineasFacPK.linea = :linea")
    , @NamedQuery(name = "LineasFac.findByCant", query = "SELECT l FROM LineasFac l WHERE l.cant = :cant")
    , @NamedQuery(name = "LineasFac.findByPrecio", query = "SELECT l FROM LineasFac l WHERE l.precio = :precio")
    , @NamedQuery(name = "LineasFac.findByDto", query = "SELECT l FROM LineasFac l WHERE l.dto = :dto")})
public class LineasFac implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected LineasFacPK lineasFacPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cant")
    private int cant;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "precio")
    private BigDecimal precio;
    @Column(name = "dto")
    private Short dto;
    @JoinColumn(name = "codart", referencedColumnName = "codart")
    @ManyToOne(optional = false)
    private Articulos codart;
    @JoinColumn(name = "codfac", referencedColumnName = "codfac", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Facturas facturas;

    public LineasFac() {
    }

    public LineasFac(LineasFacPK lineasFacPK) {
        this.lineasFacPK = lineasFacPK;
    }

    public LineasFac(LineasFacPK lineasFacPK, int cant, BigDecimal precio) {
        this.lineasFacPK = lineasFacPK;
        this.cant = cant;
        this.precio = precio;
    }

    public LineasFac(int codfac, short linea) {
        this.lineasFacPK = new LineasFacPK(codfac, linea);
    }

    public LineasFacPK getLineasFacPK() {
        return lineasFacPK;
    }

    public void setLineasFacPK(LineasFacPK lineasFacPK) {
        this.lineasFacPK = lineasFacPK;
    }

    public int getCant() {
        return cant;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public Short getDto() {
        return dto;
    }

    public void setDto(Short dto) {
        this.dto = dto;
    }

    public Articulos getCodart() {
        return codart;
    }

    public void setCodart(Articulos codart) {
        this.codart = codart;
    }

    public Facturas getFacturas() {
        return facturas;
    }

    public void setFacturas(Facturas facturas) {
        this.facturas = facturas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lineasFacPK != null ? lineasFacPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LineasFac)) {
            return false;
        }
        LineasFac other = (LineasFac) object;
        if ((this.lineasFacPK == null && other.lineasFacPK != null) || (this.lineasFacPK != null && !this.lineasFacPK.equals(other.lineasFacPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.umanizales.entidad.LineasFac[ lineasFacPK=" + lineasFacPK + " ]";
    }
    
}
