/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umanizales.entidad;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ngrajales
 */
@Entity
@Table(name = "pueblos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pueblos.findAll", query = "SELECT p FROM Pueblos p")
    , @NamedQuery(name = "Pueblos.findByCodpue", query = "SELECT p FROM Pueblos p WHERE p.codpue = :codpue")
    , @NamedQuery(name = "Pueblos.findByNombre", query = "SELECT p FROM Pueblos p WHERE p.nombre = :nombre")})
public class Pueblos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 5)
    @Column(name = "codpue")
    private String codpue;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "nombre")
    private String nombre;
    @JoinColumn(name = "codpro", referencedColumnName = "codpro")
    @ManyToOne(optional = false)
    private Provincias codpro;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codpue")
    private List<Clientes> clientesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codpue")
    private List<Vendedores> vendedoresList;

    public Pueblos() {
    }

    public Pueblos(String codpue) {
        this.codpue = codpue;
    }

    public Pueblos(String codpue, String nombre) {
        this.codpue = codpue;
        this.nombre = nombre;
    }

    public String getCodpue() {
        return codpue;
    }

    public void setCodpue(String codpue) {
        this.codpue = codpue;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Provincias getCodpro() {
        return codpro;
    }

    public void setCodpro(Provincias codpro) {
        this.codpro = codpro;
    }

    @XmlTransient
    public List<Clientes> getClientesList() {
        return clientesList;
    }

    public void setClientesList(List<Clientes> clientesList) {
        this.clientesList = clientesList;
    }

    @XmlTransient
    public List<Vendedores> getVendedoresList() {
        return vendedoresList;
    }

    public void setVendedoresList(List<Vendedores> vendedoresList) {
        this.vendedoresList = vendedoresList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codpue != null ? codpue.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pueblos)) {
            return false;
        }
        Pueblos other = (Pueblos) object;
        if ((this.codpue == null && other.codpue != null) || (this.codpue != null && !this.codpue.equals(other.codpue))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre;
    }
    
}
