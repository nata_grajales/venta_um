/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umanizales.entidad;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author ngrajales
 */
@Entity
@Table(name = "vendedores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vendedores.findAll", query = "SELECT v FROM Vendedores v")
    , @NamedQuery(name = "Vendedores.findByCodven", query = "SELECT v FROM Vendedores v WHERE v.codven = :codven")
    , @NamedQuery(name = "Vendedores.findByNombre", query = "SELECT v FROM Vendedores v WHERE v.nombre = :nombre")
    , @NamedQuery(name = "Vendedores.findByDireccion", query = "SELECT v FROM Vendedores v WHERE v.direccion = :direccion")
    , @NamedQuery(name = "Vendedores.findByCodpostal", query = "SELECT v FROM Vendedores v WHERE v.codpostal = :codpostal")})
public class Vendedores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "codven")
    private Integer codven;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 5)
    @Column(name = "codpostal")
    private String codpostal;
    @OneToMany(mappedBy = "codven")
    private List<Facturas> facturasList;
    @JoinColumn(name = "codpue", referencedColumnName = "codpue")
    @ManyToOne(optional = false)
    private Pueblos codpue;
    @OneToMany(mappedBy = "codjefe")
    private List<Vendedores> vendedoresList;
    @JoinColumn(name = "codjefe", referencedColumnName = "codven")
    @ManyToOne
    private Vendedores codjefe;

    public Vendedores() {
    }

    public Vendedores(Integer codven) {
        this.codven = codven;
    }

    public Vendedores(Integer codven, String nombre, String direccion) {
        this.codven = codven;
        this.nombre = nombre;
        this.direccion = direccion;
    }

    public Integer getCodven() {
        return codven;
    }

    public void setCodven(Integer codven) {
        this.codven = codven;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCodpostal() {
        return codpostal;
    }

    public void setCodpostal(String codpostal) {
        this.codpostal = codpostal;
    }

    @XmlTransient
    public List<Facturas> getFacturasList() {
        return facturasList;
    }

    public void setFacturasList(List<Facturas> facturasList) {
        this.facturasList = facturasList;
    }

    public Pueblos getCodpue() {
        return codpue;
    }

    public void setCodpue(Pueblos codpue) {
        this.codpue = codpue;
    }

    @XmlTransient
    public List<Vendedores> getVendedoresList() {
        return vendedoresList;
    }

    public void setVendedoresList(List<Vendedores> vendedoresList) {
        this.vendedoresList = vendedoresList;
    }

    public Vendedores getCodjefe() {
        return codjefe;
    }

    public void setCodjefe(Vendedores codjefe) {
        this.codjefe = codjefe;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codven != null ? codven.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vendedores)) {
            return false;
        }
        Vendedores other = (Vendedores) object;
        if ((this.codven == null && other.codven != null) || (this.codven != null && !this.codven.equals(other.codven))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.umanizales.entidad.Vendedores[ codven=" + codven + " ]";
    }
    
}
